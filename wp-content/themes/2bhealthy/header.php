<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>


<!--
/**
 * @license
 * MyFonts Webfont Build ID 3328369, 2017-01-06T17:47:11-0500
 * 
 * The fonts listed in this notice are subject to the End User License
 * Agreement(s) entered into by the website owner. All other parties are 
 * explicitly restricted from using the Licensed Webfonts(s).
 * 
 * You may obtain a valid license at the URLs below.
 * 
 * Webfont: AvenirLTStd-Heavy by Linotype
 * URL: http://www.myfonts.com/fonts/linotype/avenir/85-heavy/
 * 
 * Webfont: AvenirLTStd-Roman by Linotype
 * URL: http://www.myfonts.com/fonts/linotype/avenir/55-roman/
 * 
 * Webfont: AvenirLTStd-Light by Linotype
 * URL: http://www.myfonts.com/fonts/linotype/avenir/35-light/
 * 
 * 
 * License: http://www.myfonts.com/viewlicense?type=web&buildid=3328369
 * Licensed pageviews: 250,000
 * Webfonts copyright: Copyright &#x00A9; 1989, 1995, 2002 Adobe Systems Incorporated. 
 * All Rights Reserved. &#x00A9; 1981, 1995, 2002 Heidelberger Druckmaschinen AG. All
 * rights reserved.
 * 
 * © 2017 MyFonts Inc
*/

-->

<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/fonts/MyFontsWebfontsKit.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/styles/mobile-styles.css">



<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div id="wrapper" class="hfeed">
<header id="header" role="banner">
<div id="header-outer-wrapper">
<div id="header-inner-wrapper">
<div id="nav-wrapper">
<section id="branding">
<div id="site-title"><?php if ( is_front_page() || is_home() || is_front_page() && is_home() ) { echo '<h1>'; } ?><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_html( get_bloginfo( 'name' ) ); ?>" rel="home"><img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" id="site-logo" /></a><?php if ( is_front_page() || is_home() || is_front_page() && is_home() ) { echo '</h1>'; } ?></div>


</section>
<nav id="menu" role="navigation">
<?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>
</nav>

<a href="#menu" id="hamburger-menu">
  &#9776;
</a>

<br style="clear:both" />
</div>
</div>
</div>
</header>
<div id="container">