<?php /* Template Name: News */ ?>

<?php get_header(); ?>

	<section id="content" role="main">	

	<?php $args = array( 'post_type' => 'post') ?>
		<?php $loop = new WP_Query( $args );
		while ( $loop->have_posts() ) : $loop->the_post(); ?>


			<div class="news-wrapper">
										<?php $category = get_field('post_category'); ?>
			
				<?php if( $category && in_array('nutrition', $category)) { ?>
					<div class="sprite silverware" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/svg/silverware.svg'); background-size:100%;
background-position-y: 0%;" ></div>
					<?php } ?>
					
					<?php if( $category && in_array('general-health', $category)) { ?>
					<div class="sprite health" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/health.png');" ></div>
					<?php } ?>
					
					<?php if( $category && in_array('mental-health', $category)) { ?>
					<div class="sprite mental-health" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/mental.png');" ></div>
					<?php } ?>
					
					<?php if( $category && in_array('fitness', $category)) { ?>
					<div class="sprite fitness" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/fitness.png');" ></div>
					<?php } ?>	
	

				<div style="background-image: url('<?php the_post_thumbnail_url(); ?>');" class="news-post-thumbnail" ></div>
				<div class="news-inner-wrapper">
					<h3 class="newsfeed-post-title"><?php the_title(); ?></h3>
					<?php the_excerpt(); ?>
				</div>
				<br style="clear:both" />
				<a class="read-more" href="<?php the_permalink(); ?>">more</a>
			</div>
		<?php endwhile; ?>

	
	</section>
<?php get_footer(); ?>