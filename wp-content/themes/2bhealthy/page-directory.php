<?php /* Template Name: Neighborhood Directory */ ?>

<?php get_header(); ?>




	<section id="content" role="main" style="margin-top:30px;">
	<section id="the-partners">

			<ul class="the-partners-list">
<div id="partners-list-wrapper">
				<a href="#" class="partner active" id="category-0">
			  		
			  		<li>All Resources<b class="notch"></b></li>
			  	</a>
			  	<a href="#" class="partner inactive" id="category-1">
			  		<div class="sprite silverware" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/silverware.png');" ></div>
			  		<li>Nutrition Resources<b class="notch"></b></li>
			  		
			  	</a>
				<a href="#" class="partner inactive" id="category-2">
					<div class="sprite health" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/health.png');" ></div>
					<li>General Health Resources<b class="notch"></b></li>
				</a>
				<a href="#" class="partner inactive" id="category-3">
					<div class="sprite fitness" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/fitness.png');" ></div>
					<li>Fitness Resources<b class="notch"></b></li>
				</a>
				<a href="#" class="partner inactive" id="category-4">
					<div class="sprite mental" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/mental.png');" ></div>
					<li>Mental Health Resources<b class="notch"></b></li>
				</a>
</div>
			</ul>
	</section>
		
	<section id="selected-partner-info">

<div id="directory-top-nav">
	<div class="directory-top-nav-inner-wrapper">
		<section id="alpha-nav">

	<?php $terms = get_terms( 'alpha', 'hide_empty=0' );
	if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
   		 echo '<ul>';
   		 foreach ( $terms as $term ) {
   		 	if ($term->count != 0) {
   		 	echo '<li class=haspost><a href=#' . $term->name . ' class=' . $term->name . '>' . $term->name . '</a></li>';
   		 	}
   		 	if ($term->count == 0) {
   		 	echo '<li class=empty>' . $term->name . '</li>';
   		 	}
       			 
   		 }
    		echo '</ul>';
	} ?>


		</section>

<section id="search-bar">
<?php echo do_shortcode('[wpdreams_ajaxsearchlite]'); ?>
</section>

<br style="clear:both" />
</div>
</div>

	
	<div id="partner-info-wrapper">

		<div class="partner-wrapper inactive" id="category-search">
		<?php 
    		add_action( 'wp_ajax_load_search_results', 'load_search_results' );
add_action( 'wp_ajax_nopriv_load_search_results', 'load_search_results' );

function load_search_results() {
    $query = $_POST['query'];
    
    $args = array(
        'post_type' => 'tribe_organizer',
        's' => $query
    );
    $search = new WP_Query( $args );
    
    ob_start();
    
    if ( $search->have_posts() ) : 
    
    ?>

		<header class="page-header">
			<h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'twentyfourteen' ), get_search_query() ); ?></h1>
		</header>

		<?php
			while ( $search->have_posts() ) : $search->the_post();
				get_template_part( 'content', get_post_format() );
			endwhile;
			twentyfourteen_paging_nav();
	else :
		echo('Sorry, there are no results.');
	endif;
	
	$content = ob_get_clean();
	
	echo $content;
	die();
			
} ?>
			
		</div>

	
		<div class="partner-wrapper active" id="category-0">
		<?php 
		$args = array( 
		'post_type' => 'tribe_organizer', 'orderby'=> 'title', 'order' => 'ASC', 'posts_per_page' => -1,
			); 
		$loop = new WP_Query( $args );
		while ( $loop->have_posts() ) : $loop->the_post(); 
		$letter = wp_get_post_terms($post->ID, 'alpha');
		?>
		
			
					<a href="<?php the_permalink(); ?>">
					<div class="partner-info directory" id="<?php echo $letter[0]->slug; ?>" >
						<h2 class="tribe-events-page-title"><?php the_title(); ?></h2>
						<?php echo get_field('organizer_address'); ?>
					</div>
					</a>
			
			
			
		<?php endwhile; ?>
		</div>
	
		<div class="partner-wrapper inactive" id="category-1">
		<?php 
		$args = array( 
		'post_type' => 'tribe_organizer', 'orderby'=> 'title', 'order' => 'ASC', 'posts_per_page' => -1,
		'meta_query'	=> array( array(
				'key'	  	=> 'organizer_categories',
				'compare' 	=> 'LIKE',
				'value'	  	=> 'nutrition-resources',
			)),
			); 
		$loop = new WP_Query( $args );
		while ( $loop->have_posts() ) : $loop->the_post(); 
		$letter = wp_get_post_terms($post->ID, 'alpha');
		?>
		
			
					<a href="<?php the_permalink(); ?>">
					<div class="partner-info directory" id="<?php echo $letter[0]->slug; ?>">
						<h2 class="tribe-events-page-title"><?php the_title(); ?></h2>
						<?php echo get_field('organizer_address'); ?>
					</div>
					</a>
			
			
			
		<?php endwhile; ?>
		</div>
		
		<div class="partner-wrapper inactive" id="category-2">
		<?php 
		$args = array( 
		'post_type' => 'tribe_organizer', 'orderby'=> 'title', 'order' => 'ASC', 'posts_per_page' => -1,
		'meta_query'	=> array( array(
				'key'	  	=> 'organizer_categories',
				'compare' 	=> 'LIKE',
				'value'	  	=> 'general-health-resources',
			)),
			); 
		$loop = new WP_Query( $args );
		while ( $loop->have_posts() ) : $loop->the_post(); 
		$letter = wp_get_post_terms($post->ID, 'alpha');
		?>
		
			
					<a href="<?php the_permalink(); ?>">
					<div class="partner-info directory" id="<?php echo $letter[0]->slug; ?>">
						<h2 class="tribe-events-page-title"><?php the_title(); ?></h2>
						<?php echo get_field('organizer_address'); ?>
					</div>
					</a>
		<?php endwhile; ?>
		</div>
		
		
		
		<div class="partner-wrapper inactive" id="category-3">
		<?php 
		$args = array( 
		'post_type' => 'tribe_organizer', 'orderby'=> 'title', 'order' => 'ASC', 'posts_per_page' => -1,
		'meta_query'	=> array( array(
				'key'	  	=> 'organizer_categories',
				'compare' 	=> 'LIKE',
				'value'	  	=> 'fitness-resources',
			)),
			); 
		$loop = new WP_Query( $args );
		while ( $loop->have_posts() ) : $loop->the_post();
		$letter = wp_get_post_terms($post->ID, 'alpha'); 
		?>
			
			<a href="<?php the_permalink(); ?>">
					<div class="partner-info directory" id="<?php echo $letter[0]->slug; ?>">
						<h2 class="tribe-events-page-title"><?php the_title(); ?></h2>
						<?php echo get_field('organizer_address'); ?>
					</div>
			</a>
			
		<?php endwhile; ?>
		</div>
		
		
		
		<div class="partner-wrapper inactive" id="category-4">
		<?php 
		$args = array( 
		'post_type' => 'tribe_organizer', 'orderby'=> 'title', 'order' => 'ASC', 'posts_per_page' => -1,
		'meta_query'	=> array( array(
				'key'	  	=> 'organizer_categories',
				'compare' 	=> 'LIKE',
				'value'	  	=> 'mental-health-resources',
			)),
			); 
		$loop = new WP_Query( $args );
		while ( $loop->have_posts() ) : $loop->the_post(); 
		$letter = wp_get_post_terms($post->ID, 'alpha');
		?>
		
			
			<a href="<?php the_permalink(); ?>">		
					<div class="partner-info directory" id="<?php echo $letter[0]->slug; ?>">
						<h2 class="tribe-events-page-title"><?php the_title(); ?></h2>
						<?php echo get_field('organizer_address'); ?>
					</div>
			</a>
		<?php endwhile; ?>
		</div>
	
	
	
	
	</div>
	
		

	</section>
	
	</section>


<script src="<?php echo get_template_directory_uri(); ?>/scripts/directory.js"></script>

<?php get_footer(); ?>