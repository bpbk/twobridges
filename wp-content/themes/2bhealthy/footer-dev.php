<div class="clear"></div>
</div>
<footer id="footer" role="contentinfo">
	<div id="footer-inner-wrapper" style="display:flex">
<a href="/login/" id="login"/>Login</a>


<!-- Begin MailChimp Signup Form -->
<link href="//cdn-images.mailchimp.com/embedcode/horizontal-slim-10_7.css" rel="stylesheet" type="text/css">
<style type="text/css">
	#mc_embed_signup{clear:left; font:14px Helvetica,Arial,sans-serif; width:100%;}
</style>
<div id="mc_embed_signup">
<form action="//devonbussell.us16.list-manage.com/subscribe/post?u=533d51b96991bf1a55157eae4&amp;id=79e0f95993" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
    <div id="mc_embed_signup_scroll">
	<label for="mce-EMAIL">Subscribe to our mailing list!</label>
	<input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="email address" required>
    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_533d51b96991bf1a55157eae4_79e0f95993" tabindex="-1" value=""></div>
    <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
    </div>
</form>
</div>

<!--End mc_embed_signup-->


		<div id="footer-social">
		<a href="#" class="social-link" alt="Facebook" target="_blank" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/facebook.jpg');" ></a>
		
		<a href="#" class="social-link" alt="Twitter" target="_blank" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/twitter.jpg');"></a>
		
		<a href="#" class="social-link" alt="Instagram" target="_blank" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/instagram.jpg');"></a>
		
		</div>
		
		<br style="clear: both;" />	
		
	

</div>			 		

</div>
</footer>
</div>
<?php wp_footer(); ?>
<script src="<?php echo get_template_directory_uri(); ?>/scripts/allmy.js"></script>
</body>
</html>