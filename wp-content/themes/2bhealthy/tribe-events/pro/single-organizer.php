<?php
/**
 * Single Organizer Template
 * The template for an organizer. By default it displays organizer information and lists
 * events that occur with the specified organizer.
 *
 * This view contains the filters required to create an effective single organizer view.
 *
 * You can recreate an ENTIRELY new single organizer view by doing a template override, and placing
 * a Single_Organizer.php file in a tribe-events/pro/ directory within your theme directory, which
 * will override the /views/pro/single_organizer.php.
 *
 * You can use any or all filters included in this file or create your own filters in
 * your functions.php. In order to modify or extend a single filter, please see our
 * readme on templates hooks and filters (TO-DO)
 *
 * @package TribeEventsCalendarPro
 *
 * @version 4.3
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

$organizer_id = get_the_ID();
$organizer_address = get_field('organizer_address');
$organizer_address = strip_tags($organizer_address);
// $organizer_address = implode("+", preg_split("/[\s]+/", $organizer_address));
?>

<script src="<?php echo get_template_directory_uri(); ?>/scripts/map-base.js"></script>


<?php while ( have_posts() ) : the_post(); ?>




	<div class="tribe-events-organizer">
			
		
	
	
			<div class="event-column-left">
			

    <div id="map"></div>
 						

		
			</div>

	<div class="event-column-right">
		<?php do_action( 'tribe_events_single_organizer_before_organizer' ) ?>
		<div class="tribe-events-organizer-meta tribe-clearfix">

				<!-- Organizer Title -->
				<?php do_action( 'tribe_events_single_organizer_before_title' ) ?>
				<h2 class="tribe-organizer-name"><?php echo tribe_get_organizer( $organizer_id ); ?></h2>
				<?php do_action( 'tribe_events_single_organizer_after_title' ) ?>

				<!-- Organizer Meta -->
				<?php do_action( 'tribe_events_single_organizer_before_the_meta' ); ?>
				<?php echo tribe_get_organizer_details(); ?>
				<?php do_action( 'tribe_events_single_organizer_after_the_meta' ) ?>

				<!-- Organizer Featured Image 
				<?php echo tribe_event_featured_image( null, 'full' ) ?>
				-->

				<!-- Organizer Content -->
				<?php if ( get_the_content() ) { ?>
				<div class="tribe-organizer-description tribe-events-content">
					<?php the_content(); ?>
<?php if( get_field("facebook") || get_field("twitter") || get_field("instagram") ){ ?>
				 	<h4>Contact or follow us!</h4>
				 	<?php } ?>
				 	
				 		<?php 
				 		$value = get_field( "facebook" ); if( $value ) { ?>
				 		<a href="<?php echo $value; ?>" class="social-link" alt="Facebook" target="_blank" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/facebook.jpg');" ></a>
				 		<?php } else { } ?>
				 		
				 		<?php 
				 		$value = get_field( "twitter" ); if( $value ) { ?>
				 		<a href="<?php echo $value; ?>" class="social-link" alt="Twitter" target="_blank" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/twitter.jpg');"></a>
				 		<?php } else { } ?>
				 		
				 		<?php 
				 		$value = get_field( "instagram" ); if( $value ) { ?>
				 		<a href="<?php echo $value; ?>" class="social-link" alt="Instagram" target="_blank" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/instagram.jpg');"></a>
				 		<?php } else { } ?>
				 	
				</div>
				<?php } ?>

			</div>
	</div>
<br style="clear:both" />
			<!-- .tribe-events-organizer-meta -->
		<?php do_action( 'tribe_events_single_organizer_after_organizer' ) ?>

		<!-- Upcoming event list -->
		<?php do_action( 'tribe_events_single_organizer_before_upcoming_events' ) ?>

		<?php
		// Use the tribe_events_single_organizer_posts_per_page to filter the number of events to get here.
		echo tribe_organizer_upcoming_events( $organizer_id ); ?>

		<?php do_action( 'tribe_events_single_organizer_after_upcoming_events' ) ?>

	</div><!-- .tribe-events-organizer -->
	<?php
	do_action( 'tribe_events_single_organizer_after_template' );
endwhile; ?>


 <script>
      function geocodeAddress(geocoder, resultsMap) {
        var address = <?php echo json_encode($organizer_address); ?>;
        geocoder.geocode({'address': address}, function(results, status) {
          if (status === 'OK') {
            resultsMap.setCenter(results[0].geometry.location);
            var marker = new google.maps.Marker({
              map: resultsMap,
              position: results[0].geometry.location
            });
          } else {
            alert('Geocode was not successful for the following reason: ' + status);
          }
        });
        
 
      }     
</script>

    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD-3Kc9eCV7CyqXaKrykKmeQ7NbGocN0B4&callback=initMap">
    </script>