<?php
/**
 * Single Event Template
 * A single event. This displays the event title, description, meta, and
 * optionally, the Google map for the event.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/single-event.php
 *
 * @package TribeEventsCalendar
 * @version  4.3
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

$events_label_singular = tribe_get_event_label_singular();
$events_label_plural = tribe_get_event_label_plural();

$event_id = get_the_ID();

$venue_address = tribe_get_address ( $event_id );
?>

<script src="<?php echo get_template_directory_uri(); ?>/scripts/map-base.js"></script>

<div id="tribe-events-content" class="tribe-events-single">

	<!--
<p class="tribe-events-back">
		<a href="<?php echo esc_url( tribe_get_events_link() ); ?>"> <?php printf( '&laquo; ' . esc_html_x( 'All %s', '%s Events plural label', 'the-events-calendar' ), $events_label_plural ); ?></a>
	</p>
-->

	<!-- Notices -->
	<?php tribe_the_notices() ?>

	

	<!-- Event header -->
	<div id="tribe-events-header" <?php tribe_events_the_header_attributes() ?>>
		<!-- Navigation -->
		<h3 class="tribe-events-visuallyhidden"><?php printf( esc_html__( '%s Navigation', 'the-events-calendar' ), $events_label_singular ); ?></h3>
		<ul class="tribe-events-sub-nav">
			<li class="tribe-events-nav-previous"><?php tribe_the_prev_event_link( '<span>&laquo;</span> %title%' ) ?></li>
			<li class="tribe-events-nav-next"><?php tribe_the_next_event_link( '%title% <span>&raquo;</span>' ) ?></li>
		</ul>
		<!-- .tribe-events-sub-nav -->
	</div>
	<!-- #tribe-events-header -->

	<?php while ( have_posts() ) :  the_post(); ?>
		<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		
<?php $category = get_field('event_category', $event_id); ?>
			
				<?php if( $category == 'nutrition') { ?>
					<div class="sprite silverware" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/silverware.png'); background-position: right 50%; position:absolute; right:0px;" ></div>
					<?php } ?>
					
					<?php if( $category == 'general-health') { ?>
					<div class="sprite health" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/health.png'); background-position: right 50%; position:absolute; right:0px;" ></div>
					<?php } ?>
					
					<?php if( $category == 'mental-health') { ?>
					<div class="sprite mental-health" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/mental.png');background-position: right 50%; position:absolute; right:0px;" ></div>
					<?php } ?>
					
					<?php if( $category == 'fitness') { ?>
					<div class="sprite fitness" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/fitness.png'); background-position: right 50%; position:absolute; right:0px;" ></div>
					<?php } ?>
		
		<div class="event-column-left">


			<!-- Event featured image, but exclude link -->
			<?php echo tribe_event_featured_image( $event_id, 'full', false ); ?>
			
			<!-- Event meta -->
			<div id="map"></div>
			
			
			
		</div>
		
		<div class="event-column-right">
			<?php the_title( '<h1 class="tribe-events-single-event-title">', '</h1>' ); ?>
			<div class="tribe-events-schedule tribe-clearfix">
		<?php echo tribe_events_event_schedule_details( $event_id, '<h2 class="event-schedule">', '</h2>' ); ?>
		<?php
		$organizer_ids = tribe_get_organizer_ids();
		$multiple = count( $organizer_ids ) > 1;
		foreach ( $organizer_ids as $organizer ) {?>
		<div class="organizer-link">
		<?php 
		echo tribe_get_organizer_link( $organizer ); 
		echo $venue_address; ?> 
		</div>
		<?php } ?>

		<?php if ( tribe_get_cost() ) : ?>
			<span class="tribe-events-cost"><?php echo tribe_get_cost( null, true ) ?></span>
		<?php endif; ?>
	</div>
			<!-- Event content -->
			<?php do_action( 'tribe_events_single_event_before_the_content' ) ?>
			<div class="tribe-events-single-event-description tribe-events-content">
				<?php the_content(); ?>
			</div>
			<!-- .tribe-events-single-event-description -->
			<?php do_action( 'tribe_events_single_event_after_the_content' ) ?>

			
		</div> <!-- #post-x -->
		<?php if ( get_post_type() == Tribe__Events__Main::POSTTYPE && tribe_get_option( 'showComments', false ) ) comments_template() ?>
	<?php endwhile; ?>
	
	<br style="clear: both" />

	<!-- Event footer -->
	<div id="tribe-events-footer">
		<!-- Navigation -->
		<h3 class="tribe-events-visuallyhidden"><?php printf( esc_html__( '%s Navigation', 'the-events-calendar' ), $events_label_singular ); ?></h3>
		<ul class="tribe-events-sub-nav">
			<li class="tribe-events-nav-previous"><?php tribe_the_prev_event_link( '<span>&laquo;</span> %title%' ) ?></li>
			<li class="tribe-events-nav-next"><?php tribe_the_next_event_link( '%title% <span>&raquo;</span>' ) ?></li>
		</ul>
		<!-- .tribe-events-sub-nav -->
	</div>
	
	
	
	<script>
      function geocodeAddress(geocoder, resultsMap) {
        var address = <?php echo json_encode($venue_address); ?>;
        geocoder.geocode({'address': address}, function(results, status) {
          if (status === 'OK') {
            resultsMap.setCenter(results[0].geometry.location);
            var marker = new google.maps.Marker({
              map: resultsMap,
              position: results[0].geometry.location
            });
          } else {
            alert('Geocode was not successful for the following reason: ' + status);
          }
        });
        
 
      }     
</script>
	

    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD-3Kc9eCV7CyqXaKrykKmeQ7NbGocN0B4&callback=initMap">
    </script>
    
    
	<!-- #tribe-events-footer -->

</div><!-- #tribe-events-content -->