<?php
/**
 * Photo View Single Event
 * This file contains one event in the photo view
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/pro/photo/single_event.php
 *
 * @package TribeEventsCalendar
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
} ?>

<?php

global $post;

?>

<div class="tribe-events-photo-event-wrap">

	<?php echo tribe_event_featured_image( null, 'medium' ); ?>

	<div class="tribe-events-event-details tribe-clearfix">

		<!-- Event Title -->
		<?php do_action( 'tribe_events_before_the_event_title' ); ?>
		<div class="another-wrapper">
		<h2 class="tribe-events-list-event-title">
<?php $category = get_field('event_category'); ?>
			
				<?php if( $category == 'nutrition') { ?>
					<div class="sprite silverware" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/silverware.png');" ></div>
					<?php } ?>
					
					<?php if( $category == 'general-health') { ?>
					<div class="sprite health" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/health.png');" ></div>
					<?php } ?>
					
					<?php if( $category == 'mental-health') { ?>
					<div class="sprite mental-health" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/mental.png');" ></div>
					<?php } ?>
					
					<?php if( $category == 'fitness') { ?>
					<div class="sprite fitness" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/fitness.png');" ></div>
					<?php } ?>
					
			<a class="tribe-event-url" href="<?php echo esc_url( tribe_get_event_link() ); ?>" title="<?php the_title() ?>" rel="bookmark">
				<?php the_title(); ?>
			</a>
		</h2>
		<?php do_action( 'tribe_events_after_the_event_title' ); ?>

		<!-- Event Meta -->
		<?php do_action( 'tribe_events_before_the_meta' ); ?>
		<div class="tribe-events-event-meta">
			<div class="tribe-event-schedule-details">
				<?php if ( ! empty( $post->distance ) ) : ?>
					<strong>[<?php echo tribe_get_distance_with_unit( $post->distance ); ?>]</strong>
				<?php endif; ?>
				<?php echo tribe_events_event_schedule_details(); ?>
			</div>
		</div><!-- .tribe-events-event-meta -->
		<?php do_action( 'tribe_events_after_the_meta' ); ?>

		<!-- Event Content -->
		<?php do_action( 'tribe_events_before_the_content' ); ?>
		<div class="tribe-events-list-photo-description tribe-events-content">
		
		</div>
		<?php do_action( 'tribe_events_after_the_content' ) ?>
		</div>

	</div><!-- /.tribe-events-event-details -->

</div><!-- /.tribe-events-photo-event-wrap -->