<?php
/**
 * List View Single Event
 * This file contains one event in the list view
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/list/single-event.php
 *
 * @package TribeEventsCalendar
 * @version  4.3
 *
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

// Setup an array of venue details for use later in the template
$venue_details = tribe_get_venue_details();

// Venue
$has_venue_address = ( ! empty( $venue_details['address'] ) ) ? ' location' : '';

// Organizer
$organizer = tribe_get_organizer();

?>

<!-- Event Image -->

<!-- Event Cost -->
<?php if ( tribe_get_cost() ) : ?>
	<div class="tribe-events-event-cost">
		<span><?php echo tribe_get_cost( null, true ); ?></span>
	</div>
<?php endif; ?>

<?php echo tribe_event_featured_image( null, 'medium' ) ?>

<div class="event-list-inner-wrapper">



<!-- Event Title -->
<?php do_action( 'tribe_events_before_the_event_title' ) ?>
	


<?php $category = get_field('event_category'); ?>
			
				<?php if( $category == 'nutrition') { ?>
					<div class="sprite silverware white" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/silverware.png'); background-position-y:100%;" ></div>
					<?php } ?>
					
					<?php if( $category == 'general-health') { ?>
					<div class="sprite health white" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/health.png'); background-position-y:100%;" ></div>
					<?php } ?>
					
					<?php if( $category == 'mental-health') { ?>
					<div class="sprite mental-health white" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/mental.png'); background-position-y:100%;" ></div>
					<?php } ?>
					
					<?php if( $category == 'fitness') { ?>
					<div class="sprite fitness white" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/fitness.png'); background-position-y:100%;" ></div>
					<?php } ?>

<h2 class="tribe-events-list-event-title">
	<a class="tribe-event-url" href="<?php echo esc_url( tribe_get_event_link() ); ?>" title="<?php the_title_attribute() ?>" rel="bookmark">
		<?php the_title() ?>
	</a>
</h2>
<?php do_action( 'tribe_events_after_the_event_title' ) ?>

<!-- Event Meta -->
<?php do_action( 'tribe_events_before_the_meta' ) ?>
<div class="tribe-events-event-meta">
	<div class="author <?php echo esc_attr( $has_venue_address ); ?>">

		<!-- Schedule & Recurrence Details -->
		<div class="tribe-event-schedule-details">
			<?php echo tribe_events_event_schedule_details() ?>
		</div>

		<?php if ( $venue_details ) : ?>
			<!-- Venue Display Info -->
			<div class="tribe-events-venue-details">
				<?php echo implode( ', ', $venue_details ); ?>
				<?php
				if ( tribe_get_map_link() ) {
					echo tribe_get_map_link_html();
				}
				?>
			</div> <!-- .tribe-events-venue-details -->
		<?php endif; ?>

	</div>
</div><!-- .tribe-events-event-meta -->
<?php do_action( 'tribe_events_after_the_meta' ) ?>



<!-- Event Content -->
<?php do_action( 'tribe_events_before_the_content' ) ?>
<div class="tribe-events-list-event-description tribe-events-content">
	<?php echo tribe_events_get_the_excerpt( null, wp_kses_allowed_html( 'post' ) ); ?>
	<a href="<?php echo esc_url( tribe_get_event_link() ); ?>" class="tribe-events-read-more" rel="bookmark"><?php esc_html_e( 'Find out more', 'the-events-calendar' ) ?></a>
</div><!-- .tribe-events-list-event-description -->
</div>
<br style="clear:both" />
<?php
do_action( 'tribe_events_after_the_content' );