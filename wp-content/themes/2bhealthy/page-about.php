<?php /* Template Name: About */ ?>



<?php get_header(); ?>
<section id="content" role="main">
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<!--
<header class="header">
<h1 class="entry-title"><?php the_title(); ?></h1> <?php edit_post_link(); ?>
</header>
-->
<section class="entry-content">

<div class="event-column-left">
<div id="map"></div>

</div>

<div class="event-column-right" >

<h1 class="entry-title"><?php the_title(); ?></h1>
<?php the_content(); ?>
						<?php 
				 		$value = get_field( "facebook" ); if( $value ) { ?>
				 		<a href="<?php echo $value; ?>" class="social-link" alt="Facebook" target="_blank" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/facebook.jpg');" ></a>
				 		<?php } else { } ?>
				 		
				 		<?php 
				 		$value = get_field( "twitter" ); if( $value ) { ?>
				 		<a href="<?php echo $value; ?>" class="social-link" alt="Twitter" target="_blank" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/twitter.jpg');"></a>
				 		<?php } else { } ?>
				 		
				 		<?php 
				 		$value = get_field( "instagram" ); if( $value ) { ?>
				 		<a href="<?php echo $value; ?>" class="social-link" alt="Instagram" target="_blank" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/instagram.jpg');"></a>
				 		<?php } else { } ?>
				 		
				 		</div>
				 		
				 		
<div class="entry-links"><?php wp_link_pages(); ?></div>
</section>
</article>
<?php if ( ! post_password_required() ) comments_template( '', true ); ?>
<?php endwhile; endif; ?>
</section>
<?php get_footer(); ?>
<script src="<?php echo get_template_directory_uri(); ?>/scripts/map-base.js"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD-3Kc9eCV7CyqXaKrykKmeQ7NbGocN0B4&callback=initMap"></script>