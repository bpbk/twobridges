    function initMap() {
    
      
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 15,
          center: {lat: 40.712085 , lng: -73.989905}
        });
        
        
        google.maps.event.addDomListener(window, 'resize', function() {
    	map.setCenter(center);
	});

          
        // Define the LatLng coordinates for the polygon's path.
        var neighborhoodCoords = [
          {lat: 40.708035, lng: -73.998928},
          {lat: 40.710117, lng: -74.001224},
          {lat: 40.711112, lng: -74.000458},
          {lat: 40.713272, lng: -73.998177},
          
          {lat: 40.714696, lng: -73.982459},
          {lat: 40.713834, lng: -73.979455},
          {lat: 40.712260, lng: -73.979976},
          {lat: 40.712211, lng: -73.980684},
          {lat: 40.710308, lng: -73.980362},
          {lat: 40.710203, lng: -73.981220},
          {lat: 40.709853, lng: -73.981263},
          
          
         
          
          {lat: 40.709153, lng: -73.988451},
         
          {lat: 40.709951, lng: -73.988761},
          {lat: 40.708767, lng: -73.997069},
          {lat: 40.708035, lng: -73.998928}
        ];

        // Construct the polygon.
        var twoBridges = new google.maps.Polygon({
          paths: neighborhoodCoords,
          strokeColor: '#FF0000',
          strokeOpacity: 0.6,
          strokeWeight: 2,
          fillColor: '#FF0000',
          fillOpacity: 0.25
        });
        twoBridges.setMap(map); 



	var geocoder = new google.maps.Geocoder();
        geocodeAddress(geocoder, map);
   
      }