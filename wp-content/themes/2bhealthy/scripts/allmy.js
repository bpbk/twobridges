$("ul.the-partners-list a.partner").click( function() {

	$the_id = $(this).attr('id');

	$("ul.the-partners-list a.partner").removeClass("active").addClass("inactive");
	$(this).removeClass("inactive").addClass("active");

	$("div.partner-wrapper").removeClass("active").addClass("inactive");
	$("div.partner-wrapper#" + $the_id).removeClass("inactive").addClass("active");

});


$( document ).ready(function() {
var header_top = $("header#header").offset().top + 120;

	$( document ).scroll(function() {
		var window_top = $(window).scrollTop();
		
		if (window_top > header_top) {
	        $('header#header').addClass('sticky');
	      	//  $('#sticky-anchor').height($('div#directory-top-nav').outerHeight());
	   	 } 
	    
	    	if (window_top < header_top) {
	    	$('header#header').removeClass('sticky');
	    	}
	
	});

});


$('a#hamburger-menu').click(function(){
	$(this).toggleClass("active");
	$('nav#menu').toggleClass("active");

});


$( window ).resize(function() {

  var newWidth = $( window ).width();
  
  if ( newWidth > 1080 ){
  $('a#hamburger-menu').removeClass("active");
  $('nav#menu').removeClass("active");
  }
  
});


/// Events Submission Form Hacks
$( document ).ready(function() {
	$('div.events-community-post-title input#post_title').attr('value','');
	$( "body.tribe_community_edit nav li a:contains('Submit')" ).css( "color", "#000" );
	
});



/* 
Partners Page 
$( document ).ready(function() {
	if( $(".partner-wrapper .tribe-events-list").has(".type-tribe_events") ) {
		$(this).find(".tribe-events-notices").css("display", "none");
	}
	else {}
});

*/