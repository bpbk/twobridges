$(function() {
  $('a[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
});


$('section#alpha-nav a').click( function(){
	$('section#alpha-nav a').removeClass("active");
	$(this).addClass("active");
});


$( document ).ready(function() {
var div_top = $("div#directory-top-nav").offset().top;

	$( document ).scroll(function() {
		var window_top = $(window).scrollTop();
		
		if (window_top > div_top) {
	        $('div#directory-top-nav').addClass('sticky');
	      	//  $('#sticky-anchor').height($('div#directory-top-nav').outerHeight());
	   	 } 
	    
	    	if (window_top < div_top) {
	    	$('div#directory-top-nav').removeClass('sticky');
	    	}
	
	});

});


$( document ).ready(function() {
var partners_top = $("section#the-partners").offset().top;

	$( document ).scroll(function() {
		var window_top = $(window).scrollTop();
		
		if (window_top > partners_top) {
	        $('section#the-partners').addClass('sticky');
	      	//  $('#sticky-anchor').height($('div#directory-top-nav').outerHeight());
	   	 } 
	    
	    	if (window_top < partners_top) {
	    	$('section#the-partners').removeClass('sticky');
	    	}
	
	});

});

/*

  	$( "ul.the-partners-list" ).on('wheel', function(e) {

	var delta = e.originalEvent.deltaY;

	if (delta > 0) {
		$("div#partners-list-wrapper").css({"-webkit-transform":"translate(0px, 0px)"})
		}
	else {
		$("div#partners-list-wrapper").css({"-webkit-transform":"translate(0px, -200px)"})
	
	}

	return false; // this line is only added so the whole page won't scroll in the demo
	});

*/	


  