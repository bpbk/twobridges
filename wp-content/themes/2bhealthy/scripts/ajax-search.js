jQuery(document).on( 'submit', 'section#search-bar form', function() {
    var $form = jQuery(this);
    var $input = $form.find('input[name="s"]');
    var query = $input.val();
    var $content = jQuery('div.partner-wrapper#category-search');
    
    jQuery.ajax({
        type : 'post',
        url : myAjax.ajaxurl,
        data : {
            action : 'load_search_results',
            query : query
        },
        beforeSend: function() {
            $input.prop('disabled', true);
            $('div.partner-wrapper').removeClass('active');
            $content.addClass('active').addClass('loading');
            
        },
        success : function( response ) {
            $input.prop('disabled', false);
            $content.removeClass('loading');
            $content.html( response );
            

        }
    });
    
    return false;
})
