<?php /* Template Name: Partners */ ?>

<?php get_header(); ?>

	<?php 
	$args = array( 
	'post_type' => 'tribe_organizer', 'orderby'=> 'title', 'order' => 'ASC',
	'meta_query'	=> array( array(
			'key'	  	=> 'partner_organization',
			'compare' 	=> '==',
			'value'	  	=> '1',
	)),
	); 
	?>


	<section id="content" role="main">	
	<section id="the-partners">
		<ul class="the-partners-list">
		<?php
		$count = 1; 
		$loop = new WP_Query( $args );
		while ( $loop->have_posts() ) : $loop->the_post(); 
		?>

			  	<a href="#" class="partner <?php if ( $count == 1 ) { echo "active" ; } ?> arrow_box" id="partner-<?php echo $count; ?>"><li><?php the_title(); ?><b class="notch"></b></li></a>
			
		<?php $count++;
		endwhile; ?>
		</ul>
	</section>
		
		
	<section id="selected-partner-info">
		<div id="partner-info-wrapper">

	
		<?php $count = 1;
		$loop = new WP_Query( $args );
		while ( $loop->have_posts() ) : $loop->the_post(); 
		$organizer_ID = tribe_get_organizer_id();
		?>

			<div class="partner-wrapper <?php if ( $count == 1 ) { echo "active" ; } ?>" id="partner-<?php echo $count; ?>" >
			
			<div class="partner-sidebar">
				<div class="partner-sidebar-wrapper">
				 	<?php the_post_thumbnail(); ?>
				 	


				 	
				 	<!-- Organizer Meta -->
				 	
					<?php $website = tribe_get_organizer_website_url(); ?>
					<div>
					<a href="<?php echo $website ?>" target="blank" class="the-website">
					<?php 
						if( get_field('short_url') ) {
						the_field('short_url');
						}
						else echo $website;
					 ?>
					
					</a>
					 </div>

					<?php echo get_field('organizer_address'); ?>
					
					<?php if( get_field("facebook") || get_field("twitter") || get_field("instagram") ){ ?>
				 	<h4 class="contact">Contact or follow us!</h4>
				 	<?php } ?>
				 	
				 		<?php 
				 		$value = get_field( "facebook" ); if( $value ) { ?>
				 		<a href="<?php echo $value; ?>" class="social-link" alt="Facebook" target="_blank" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/facebook.jpg');" ></a>
				 		<?php } else { } ?>
				 		
				 		<?php 
				 		$value = get_field( "twitter" ); if( $value ) { ?>
				 		<a href="<?php echo $value; ?>" class="social-link" alt="Twitter" target="_blank" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/twitter.jpg');"></a>
				 		<?php } else { } ?>
				 		
				 		<?php 
				 		$value = get_field( "instagram" ); if( $value ) { ?>
				 		<a href="<?php echo $value; ?>" class="social-link" alt="Instagram" target="_blank" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/instagram.jpg');"></a>
				 		<?php } else { } ?>
				 	
				 		
				</div>
				</div>
				<div class="partner-wrapper-left">	
					
					<div class="partner-info">
					<a href="<?php the_permalink(); ?>"><h3><?php the_title(); ?></h3></a>
					<?php the_content(); ?>
					</div>
					
					
					<div class="partner-events">
					<!-- Upcoming event list -->
		<?php do_action( 'tribe_events_single_organizer_before_upcoming_events' ) ?>

		<?php
		// Use the tribe_events_single_organizer_posts_per_page to filter the number of events to get here.
		echo tribe_organizer_upcoming_events( $organizer_id ); ?>

		<?php do_action( 'tribe_events_single_organizer_after_upcoming_events' ) ?>

					
					</div>
					
					
					
				

				</div>
				
				
				<br style="clear:both" />
			</div>
		<?php $count++;
		endwhile; ?>
		</div>
		
	</section>
	
	</section>
<?php get_footer(); ?>