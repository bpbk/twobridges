<?php
add_action( 'after_setup_theme', 'blankslate_setup' );
function blankslate_setup()
{
load_theme_textdomain( 'blankslate', get_template_directory() . '/languages' );
add_theme_support( 'title-tag' );
add_theme_support( 'automatic-feed-links' );
add_theme_support( 'post-thumbnails' );
global $content_width;
if ( ! isset( $content_width ) ) $content_width = 640;
register_nav_menus(
array( 'main-menu' => __( 'Main Menu', 'blankslate' ) )
);
}
add_action( 'wp_enqueue_scripts', 'blankslate_load_scripts' );
function blankslate_load_scripts()
{
wp_enqueue_script( 'jquery' );
}
add_action( 'comment_form_before', 'blankslate_enqueue_comment_reply_script' );
function blankslate_enqueue_comment_reply_script()
{
if ( get_option( 'thread_comments' ) ) { wp_enqueue_script( 'comment-reply' ); }
}
add_filter( 'the_title', 'blankslate_title' );
function blankslate_title( $title ) {
if ( $title == '' ) {
return '&rarr;';
} else {
return $title;
}
}
add_filter( 'wp_title', 'blankslate_filter_wp_title' );
function blankslate_filter_wp_title( $title )
{
return $title . esc_attr( get_bloginfo( 'name' ) );
}
add_action( 'widgets_init', 'blankslate_widgets_init' );
function blankslate_widgets_init()
{
register_sidebar( array (
'name' => __( 'Sidebar Widget Area', 'blankslate' ),
'id' => 'primary-widget-area',
'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
'after_widget' => "</li>",
'before_title' => '<h3 class="widget-title">',
'after_title' => '</h3>',
) );
}
function blankslate_custom_pings( $comment )
{
$GLOBALS['comment'] = $comment;
?>
<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>"><?php echo comment_author_link(); ?></li>
<?php
}
add_filter( 'get_comments_number', 'blankslate_comments_number' );
function blankslate_comments_number( $count )
{
if ( !is_admin() ) {
global $id;
$comments_by_type = &separate_comments( get_comments( 'status=approve&post_id=' . $id ) );
return count( $comments_by_type['comment'] );
} else {
return $count;
}
}
if ( function_exists( 'tribe_is_event') ) {
	/**
	 * Modifying the thumbnail arguments in Month view tooltip from a filter.
	 *
	 * @link https://theeventscalendar.com/support/forums/topic/month-view-tooltip-image-size/
	 * @return array
	 */
	function change_tribe_json_tooltip_thumbnail( $json ) {
		$event_id = get_the_ID();

		if ( ! empty( $json['imageTooltipSrc'] ) ) {
			$thumb_id       = get_post_thumbnail_id( $event_id );
			$thumbnail_atts = wp_get_attachment_image_src( $thumb_id, 'medium' );

			$json['imageTooltipSrc'] = $thumbnail_atts[0];
		}

		return $json;
	}

	add_filter( 'tribe_events_template_data_array', 'change_tribe_json_tooltip_thumbnail' );
}
function alphaindex_alpha_tax() {
	register_taxonomy( 'alpha',array (
		0 => 'tribe_organizer',
	),
	array( 'hierarchical' => false,
		'label' => 'Alpha',
		'show_ui' => false,
		'query_var' => true,
		'show_admin_column' => false,
	) );
}

function prefix_teammembers_metaboxes( ) {
   global $wp_meta_boxes;

}

function pluginname_ajaxurl() { ?>
	<script type="text/javascript">
		var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
		var templateurl = '<?php echo get_template_directory_uri(); ?>';
		var homeurl = '<?php echo get_home_url(); ?>';
		var siteurl = '<?php echo site_url(); ?>';
	</script>
<?php
}

//add_action('wp_ajax_nopriv_send_emails', 'send_emails_function');
add_action('wp_ajax_send_emails', 'send_emails_function');

function send_emails_function(){
    $eventid = $_REQUEST['event_id'];
	$eventSubj = $_REQUEST['event_subj'];
	$eventBody = $_REQUEST['event_body'];
	$emailType = $_REQUEST['email_type'];
	if($emailType === 'test'){
		$to = $_REQUEST['test_email'];
	}else{
		$attendees = tribe_tickets_get_attendees($eventid);
		$uniqueAttendees = unique_email_array($attendees, 'holder_email');
		$uniqueEmails = array();
		foreach($uniqueAttendees as $uEmail){
			array_push($uniqueEmails, $uEmail['holder_email']);
		}
		$to = $uniqueEmails;
	}
	$subject = $eventSubj;
	ob_start(); ?>
	<html>
		<head>
			<style>
				h1{
					font-size: 30px;
				}
				section[style="display:block"] h1{
					font-size: 30px;
				}
				section[style="display:block"]{
					padding: 30px 40px 120px;
				}
				section[style="display:block"] h2{
					font-size: 26px;
				}
				section[style="display:block"] h3{
					font-size: 22px;
				}
				section[style="display:block"] h4{
					font-size: 18px;
				}
				section[style="display:block"] h5{
					font-size: 14px;
				}
				section[style="display:block"] h6{
					font-size: 12px;
				}
				section[style="display:block"] p{
					padding-bottom: 16px;
				}
				section[style="display:block"] a{
					text-decoration: none;
					font-weight: bold;
					color: #24cffb;
				}
			</style>
		</head>
		<body>
			<header style="width:100%; background-color:#24cffb; padding: 90px 0;">
				<img style="width:247px; height:auto; display:block; position:relative; margin: 0 auto;" src="http://uncanny.studio/images/2bhealthy.png"/>
			</header>
			<section id="email_body" tbh="true" style="display:block;" >
				<?php echo $eventBody; ?>
			</section>
		</body>
	</html>
	<?php
	$body = ob_get_clean();
	$headers = array('Content-Type: text/html; charset=UTF-8');
	if(wp_mail( $to, $subject, $body, $headers )){
		echo 1;
	}else{
		echo 0;
	}

    die;
}

add_action( 'add_meta_boxes_tribe_events', 'email_attendees' );

function email_attendees(){
	add_meta_box('email_attendees', 'Email Attendees', 'email_attendees_form');
}

function unique_email_array($array, $key) {
    $temp_array = array();
    $i = 0;
    $key_array = array();

    foreach($array as $val) {
        if (!in_array($val[$key], $key_array) && $val['order_status'] === 'yes') {
            $key_array[$i] = $val[$key];
            $temp_array[$i] = $val;
        }
        $i++;
    }
    return $temp_array;
}

function email_attendees_form() {
	global $post;
	$attendees = tribe_tickets_get_attendees($post->ID);
	// replace with admin email in array
	$uniqueEmails = unique_email_array($attendees, 'holder_email');
/*
	if($attendees){
		$uniqueAttendees = array_map(function($attendee){
			if(!in_array($attendee->holder_email, $uniqueEmails) && $attendee->order_status == 'yes'){
				array_push($uniqueEmails, $attendee->holder_email);
				return $attendee;
			}
		}, $attendees);
		print_r($uniqueAttendees);
	}
*/

?>
	<style>
		.send_email_button{
			margin-top:20px;
			background-color: rgb(220,220,220);
		}
		.test_email_button{
			background-color: rgb(220,220,220);
		}
		.send_email_button.inactive_button{
			opacity: .5;
			pointer-events: none;
			cursor: default;
		}
	</style>
	<form id="event_email_form" eventid="<?php echo $post->ID; ?>">
		<label>Email subject</label>
		<input type="text" id="email_subject" style="width:100%; margin-bottom: 20px;" value="<?php echo $post->post_title; ?>">
		<?php wp_editor('<h1>'.$post->post_title.'</h1><p>This event is coming up</p>', 'email_body'); ?>
		<input type="submit" value="send to <?php echo count($uniqueEmails) !== 0 ? count($uniqueEmails) : ''; ?> attendee(s)" class="send_email_button<?php echo count($uniqueEmails) === 0 ? ' inactive_button' : ''; ?>">
		<span style="padding: 20px 0; display: block;">OR</span>
		<input type="text" id="test_email" value="<?php echo get_option('admin_email'); ?>">
		<input type="submit" value="send test email" class="test_email_button">
	</form>

	<script>
	jQuery.noConflict();
	(function( $ ) {
  	$(function() {
	  		$('.test_email_button').on('click', function(){
		  		var eID = $('#event_email_form').attr('eventid');
				var eSubj = $('#email_subject').val();
				var eBody = tinymce.editors.email_body.getContent();
				var testEmail = $('#test_email').val();
				$.ajax({
					url: ajaxurl,
					data: {
						'action': 'send_emails',
						'email_type':'test',
						'event_id': eID,
						'event_subj': eSubj,
						'event_body': eBody,
						'test_email': testEmail
					},
					//dataType: 'json',
					success: function(data) {
						//var nlJson = $.parseJSON( data );
						if(data === '1'){
							alert('sent test email');
						}else{
							alert('something went wrong! Reload and try again');
						}
					},
					complete:function(){
					},
					error: function(errorThrown) {
						console.log(errorThrown);
					}
				});
				return false;
		  	});
			$('.send_email_button:not(.inactive_button)').on('click', function(){
				var eID = $('#event_email_form').attr('eventid');
				var eSubj = $('#email_subject').val();
				var eBody = tinymce.editors.email_body.getContent();
				if(confirm('send to all attendees?')){
					$.ajax({
						url: ajaxurl,
						data: {
							'action': 'send_emails',
							'event_id': eID,
							'event_subj': eSubj,
							'event_body': eBody,
							'email_type': 'production'
						},
						//dataType: 'json',
						success: function(data) {
							//var nlJson = $.parseJSON( data );
							if(data === '1'){
								alert('emails sent to all attendees');
							}else{
								alert('something went wrong! Reload and try again');
							}
						},
						complete:function(){
						},
						error: function(errorThrown) {
							console.log(errorThrown);
						}
					});
				}else{
					return false;
				}

				return false;
			});
		});
	})(jQuery);
	</script>
<?php
}

add_action('init', 'alphaindex_alpha_tax');
function alphaindex_save_alpha( $post_id ) {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
	return;
	//only run for songs
	$slug = 'tribe_organizer';
	$letter = '';
	// If this isn't a 'song' post, don't update it.
	if ( isset( $_POST['post_type'] ) && ( $slug != $_POST['post_type'] ) )
	return;
	// Check permissions
	if ( !current_user_can( 'edit_post', $post_id ) )
	return;
	// OK, we're authenticated: we need to find and save the data
	$taxonomy = 'alpha';
	if ( isset( $_POST['post_type'] ) ) {
		// Get the title of the post
		$title = strtolower( $_POST['post_title'] );

		// The next few lines remove A, An, or The from the start of the title
		$splitTitle = explode(" ", $title);
		$blacklist = array("an","a","the");
		$splitTitle[0] = str_replace($blacklist,"",strtolower($splitTitle[0]));
		$title = implode(" ", $splitTitle);

		// Get the first letter of the title
		$letter = substr( $title, 0, 1 );

		// Set to 0-9 if it's a number
		if ( is_numeric( $letter ) ) {
			$letter = '0-9';
		}
	}
	//set term as first letter of post title, lower case
	wp_set_post_terms( $post_id, $letter, $taxonomy );
}

function login_redirect( $redirect_to, $request, $user ){
    return home_url('/');
}
add_filter( 'login_redirect', 'login_redirect', 10, 3 );



add_filter( 'tribe_events_community_required_fields', 'my_community_required_fields', 10, 1 );

function my_community_required_fields( $fields ) {

    if ( ! is_array( $fields ) ) {
        return $fields;
    }

    $fields[] = 'post_content';

    return $fields;
}
